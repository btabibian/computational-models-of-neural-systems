res=zeros(1,100);
for j=1:100
  N = 10;           % length of a key
  B = 10;            % number of 1 bits in a key
  Keys = [];
  Values = 0;       % values to store
  Results = 0;      % results of retrieval using Keys

  M = zeros(1,N);   % weight matrix

  NumPatterns = 0;
  while max(abs(Values-Results)) < 0.001
    NumPatterns = NumPatterns + 1;
    k = keyvec(N,B) / B;   % normalized random key
    Keys = [Keys k];
    Values = [1:NumPatterns];
    M = Values*pinv(Keys); %fill this in to construct the weight matrix
    Results = M*Keys; %fill this in to retrieve the values given the keys
  end
  res(j)=NumPatterns-1;
  %fprintf('Stored %d patterns without error.\n',NumPatterns-1);
end
mean(res)
