function Z = keyvec(N,B)
 % Generate an N-bit binary vector with a random B bits true
  Z=zeros(N,1);
  P = randperm(N);
  Z(P(1:B)) = 1;
